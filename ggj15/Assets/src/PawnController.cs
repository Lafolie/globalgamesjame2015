﻿using UnityEngine;
using System.Collections;

public class PawnController : MonoBehaviour {
    public Pawn posessedPawn;
	public	int IdleFacing;  // 0-Left 1-Right
	public int targetFacing = 0;
	bool canMove = true;

	void Start () 
	{

	}

	void Update ()                       
	{

	Vector3 inputMovement = new Vector3(Input.GetAxis("XAxis"), 0, Input.GetAxis("YAxis"));
        inputMovement.Normalize();

		if (canMove == true) 
		{
			posessedPawn.movement (inputMovement);
		}

		bool jumpInput = Input.GetButtonDown ("Jump");
		bool attackInput = Input.GetButtonDown ("Attack");

		if (inputMovement.x < 0) 
		{
			IdleFacing = -1;
			Flip();
		}

		if (inputMovement.x > 0) 
		{
			IdleFacing = 1;
			Flip();
		}

		if (jumpInput) 
		{                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
			posessedPawn.Jump();
		}

		if (attackInput) 
		{
			canMove = false;
			print("attack");
			posessedPawn.Attack();
			canMove = true;
		}

	}

	public void Flip()
	{
		if (IdleFacing == 1) //right
		{
			posessedPawn.transform.localScale = new Vector3(2.0f, 2.0f, 2.0f); 
		}

		if (IdleFacing == -1) //left
		{
			posessedPawn.transform.localScale = new Vector3(-2.0f, 2.0f, 2.0f); 
		}
	}
}
