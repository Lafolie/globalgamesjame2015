﻿using UnityEngine;
using System.Collections;

public class Hitbox : MonoBehaviour {
    public Vector3 direction;
    public float knockback;
    public int damage;
    public float life;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        life -= Time.deltaTime;
        if (life <= 0)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        Pawn Player = other.gameObject.GetComponent<Pawn>();
		        if (Player)
        {
			Player.TakeDamage(damage);
            Physics.IgnoreCollision(collider, other.collider);
        }
    }
}
