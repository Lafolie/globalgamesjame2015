﻿using UnityEngine;
using System.Collections;

public class SpriteClass : MonoBehaviour 
{
			int index ; // For Sprite Animation

	public float framesPerSecond; // For Sprite Animation
	 
	public Sprite[] WalkLeftSprites;
	public Sprite[] WalkRightSprites;
    public Sprite[] JumpSprites;
	public Sprite[] IdleSpritesLeft;
	public Sprite[] IdleSpritesRight;
	public Sprite[] AttackLeftSprites;
	public Sprite[] AttackRightSprites;

	private SpriteRenderer spriteRenderer;

	public virtual void Start()
	{
		spriteRenderer = renderer as SpriteRenderer;
	}

	void Update () 
	{
       
		
		index = (int)(Time.timeSinceLevelLoad * framesPerSecond);  
	}

    /*----------------------------------------------------------------------------------------*/

	public void AnimateLeft()
	{
		index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
		index = index % WalkLeftSprites.Length;
	    spriteRenderer.sprite = WalkLeftSprites[index];
	}

	public void AnimateRight ()
	{
		index = index % WalkRightSprites.Length;
		spriteRenderer.sprite = WalkRightSprites[index];
	}

	void AnimateBack ()
	{

	}

	void AnimateForwards ()
	{

	}
	
	void AnimateJump ()
    {
        spriteRenderer.sprite = JumpSprites[index];
    }

	public void AnimateIdle ()
	{
		index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
		index = index % IdleSpritesLeft.Length;
		spriteRenderer.sprite = IdleSpritesLeft [index];
	}

	public void AnimateAttackLeft ()
	{
		index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
		index = index % AttackLeftSprites.Length;
		spriteRenderer.sprite = AttackLeftSprites [index];
	}

	public void AnimateAttackRight ()
	{
		index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
		index = index % AttackRightSprites.Length;
		spriteRenderer.sprite = AttackRightSprites [index];
	}
}