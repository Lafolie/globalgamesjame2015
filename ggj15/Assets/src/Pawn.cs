﻿using UnityEngine;
using System.Collections;

public class Pawn : SpriteClass
{
    public float moveSpeed = 10.0f;
	public float MaxDepth;
	public float MinDepth;
	private float lowExtent;
	public bool OnGround;
	float coolDown;
	public override void Start ()
	{
		base.Start ();
		lowExtent = collider.bounds.extents.y + 0.1f;
	}

	void Update ()
	{
		OnGround = Physics.Raycast (transform.position, -Vector3.up, lowExtent);
		coolDown -= Time.deltaTime;
		print (coolDown);
 
		
	}

    public void movement(Vector3 position)
    {

				if (coolDown <= 0) {
						Vector3 newPosition = transform.position + moveSpeed * position * Time.deltaTime;
						transform.position = newPosition;

						if (position.x > 0.0 || position.x < 0.0) {
								AnimateLeft ();
						} else {
								AnimateIdle ();
						}
				}
		}
	public void Jump()
	{
		if (OnGround) 
		{
			rigidbody.AddForce (Vector3.up * 200f);
		}
	}

	public void Attack()
	{
		AnimateAttackLeft ();
		coolDown = 1;
	}

	public void Idle()
	{
		AnimateIdle();
	}

	public void TakeDamage(float damage)
	{

	}



}
